<?php

namespace Database\Factories;
use App\Models\Product;
use App\Models\Category;
use App\Models\CategoryProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CategoryProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
     
     //Genero los datos de prueba
    public function definition()
    {
        return [
          'category_id' => Category::All()->random()->id,
          'product_id' => Product::All()->random()->id,
        ];
    }
}
