<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

     //Genero los datos de prueba
    public function definition()
    {
      return [
          'nombre' => $this->faker->name,
          'descripcion' => $this->faker->text,
          'foto' => 'images/products/default.png',
      ];
    }
}
