<?php

namespace Database\Factories;

use App\Models\Tarifas;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class TarifasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tarifas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
     
     //Genero los datos de prueba
    public function definition()
    {
        $starting_date = $this->faker->dateTimeBetween($start_date = '-1 years', $enddate = 'now');
        $ending_date = $this->faker->dateTimeBetween($start_date = 'now', $enddate = '1 years');
      return [
          'product_id' => Product::all()->random()->id,
          'fecha_inicial' => $starting_date,
          'fecha_final' => $ending_date,
          'precio' => $this->faker->numberBetween(1,1000),
      ];
    }
}
