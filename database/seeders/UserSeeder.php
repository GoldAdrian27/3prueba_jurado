<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Generator;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=0; $i < 100 ; $i++) {
        //Llamo al factory para generar los datos
        User::testDatabase();
      }

    }
}
