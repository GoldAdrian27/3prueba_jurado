<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CategoryProduct;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=0; $i <100 ; $i++) {
        //Llamo al factory para generar los datos
        CategoryProduct::testDatabase();
      }
    }
}
