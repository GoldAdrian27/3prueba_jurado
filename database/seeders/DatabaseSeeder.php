<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      //Elimino todos las tuplas de la base de datos
      $this->truncateTables([
        'users',
        'tarifas',
        'categories',
        'products',
        'category_product',
      ]);
      //Añado tuplas a la base de datos
      $this->call([
        UserSeeder::class,
        CategorySeeder::class,
        ProductSeeder::class,
        TarifasSeeder::class,
        CategoryProductSeeder::class,
      ]);
    }
    protected function truncateTables(array $tables){

    DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
    foreach ($tables as $table) {
      DB::table($table)->truncate();
    }
    DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
