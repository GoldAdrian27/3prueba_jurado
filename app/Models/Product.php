<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function categories(){
      return $this->belongsToMany('App\Models\Category' , 'category_product');
    }
    public function tarifas(){
        return $this->hasMany('App\Models\Tarifas');
    }

    public function fotos(){
        return $this->hasMany('App\Models\FotosDeProducto');
    }

    //Alimentar la base de datos
    public static function testDatabase(){
      $product = Product::factory()->make();
      $product->save();
    }

    //Compruebo si la categoria que paso por parametro esta relacionada con este producto
    public function hasCategory($categoryId){
      return $this->categories()->where('category_id', $categoryId)->count() > 0;
    }
}
