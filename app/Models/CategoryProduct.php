<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    use HasFactory;

    protected $table ="category_product";

    //Alimentar la base de datos
    public static function testDatabase()
    {
      $product = CategoryProduct::factory()->make();
      $product->save();
    }
}
