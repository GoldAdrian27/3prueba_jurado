<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarifas extends Model
{
    use HasFactory;



    public function product(){
      return $this->belongsTo('App\Models\Product');
    }

    //Alimentar la base de datos
    public static function testDatabase()
    {
      $tarifas = Tarifas::factory()->make();
      $tarifas->save();
    }
}
