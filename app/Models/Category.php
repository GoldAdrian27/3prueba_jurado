<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function products(){
      return $this->belongsToMany('App\Models\Product' , 'category_product');
    }

    //Alimentar la base de datos
    public static function testDatabase()
    {
      $category = Category::factory()->make();
      $category->save();
    }
}
