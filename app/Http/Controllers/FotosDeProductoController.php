<?php

namespace App\Http\Controllers;

use App\Models\FotosDeProducto;
use Illuminate\Http\Request;

class FotosDeProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FotosDeProducto  $fotosDeProducto
     * @return \Illuminate\Http\Response
     */
    public function show(FotosDeProducto $fotosDeProducto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FotosDeProducto  $fotosDeProducto
     * @return \Illuminate\Http\Response
     */
    public function edit(FotosDeProducto $fotosDeProducto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FotosDeProducto  $fotosDeProducto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FotosDeProducto $fotosDeProducto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FotosDeProducto  $fotosDeProducto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $fotosDeProducto = FotosDeProducto::findOrFail($request->id);
        $this->removeImage($fotosDeProducto->url);
        $fotosDeProducto->delete();
        return redirect()->back();
    }

    public function destroyAll(Request $request)
    {
        $fotosDeProducto = FotosDeProducto::where('product_id', $request->id);
        foreach ($fotosDeProducto->get() as $foto) {
          $this->removeImage($foto->url);
        }

        $fotosDeProducto->delete();



        return redirect()->back();
    }

    public function removeImage($url)
    {
        if (\File::exists(public_path($url))) {
            \File::delete(public_path($url));
        }
    }
}
