<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\http\Requests\UserRequest;
use App\http\Requests\UserLoginRequest;
use App\http\Requests\UserEditRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UserController extends Controller
{
    //----Log in-------
    public function login(UserLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended(route('index'));
        }
        return redirect(route('login'))
             ->withErrors(['errors' => 'Contraseña o email incorrecto'])
             ->withInput();

        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function register(UserRequest $request)
    {
        $user = new User();
        $user->nombre = $request->nombre;
        $user->apellido = $request->apellido;
        $user->email = $request->email;
        $user->fecha_nacimiento = $request->fecha_nacimiento;
        //Si contiene una foto
        if ($request->hasFile('foto')) {
            $file = $request->foto;
            //Cambio el nombre a la foto con un string aleatorio y la guardo en una carpeta en public
            $random = Str::random(20);
            //Si existe esa imagen le cambio el nombre
            while (is_file(public_path() . '/images/users/' . $random . '.' . $file->getClientOriginalExtension())) {
                $random = Str::random(20);
            }

            $file->move(public_path() . '/images/users', $random . '.' . ($file->getClientOriginalExtension()));
            //Guardo la direccion de la foto en la base de datos con el mismo nombre
            $user->foto = 'images/users/' . $random . '.' . ($file->getClientOriginalExtension());
        }else{
          $user->foto = 'images/users/default.png';
        }


        $user->password = Hash::make($request->password);
        $user->save();

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended(route('index'));
        }
    }

    //-----CRUD-------

    public function index()
    {
        $users = User::All();
        return view('user.list', compact('users'));
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('user.show', compact('user'));
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(UserRequest $request)
    {
        $user = new User();
        $user->nombre = request('nombre');
        $user->apellido = request('apellido');
        $user->email = request('email');
        $user->fecha_nacimiento = request('fecha_nacimiento');
        $user->password = Hash::make($request->password);

        //Si contiene una foto
        if ($request->hasFile('foto')) {
            $file = $request->foto;
            //Cambio el nombre a la foto con un string aleatorio y la guardo en una carpeta en public
            $random = Str::random(20);
            while (is_file(public_path() . '/images/users/' . $random . '.' . $file->getClientOriginalExtension())) {
                $random = Str::random(20);
            }
            $file->move(public_path() . '/images/users', $random . '.' . ($file->getClientOriginalExtension()));
            //Guardo la direccion de la foto en la base de datos con el mismo nombre
            $user->foto = 'images/users/' . $random . '.' . ($file->getClientOriginalExtension());
        }else{
          $user->foto = 'images/users/default.png';
        }

        $user->save();

        return redirect(route('user.list'));
    }

    public function edit($id)
    {
        $user = User::FindOrFail($id);

        return view('user.edit', compact('user'));
    }

    public function update(UserEditRequest $request)
    {
        $user = User::FindOrFail(request('id'));

        if (!empty($request->nombre)) {
            $user->nombre = request('nombre');
        }
        if (!empty($request->apellido)) {
            $user->apellido = request('apellido');
        }
        if (!empty($request->email)) {
            $user->email = request('email');
        }
        if (!empty($request->fecha_nacimiento)) {
            $user->fecha_nacimiento = request('fecha_nacimiento');
        }
        if (!empty($request->password)) {
            $user->password = Hash::make($request->password);
        }

        //Si contiene una foto
        if ($request->hasFile('foto')) {
          //Estoy eliminando la foto anterior cuando esta se Modificar
          //Seria mas sencillo utilizar el mismo nombre usando la id del usuario para que la imagen se sustituya automaticamente
          //aunque si tiene otra extension (.png, .jpg, .jpeg) no funcionaria
          $this->removeImage($user->foto);
            $file = $request->foto;
            //Cambio el nombre a la foto con un string aleatorio y la guardo en una carpeta en public
            $random = Str::random(20);
            while (is_file(public_path() . '/images/users/' . $random . '.' . $file->getClientOriginalExtension())) {
                $random = Str::random(20);
            }
            $file->move(public_path() . '/images/users/' , $random . '.' . ($file->getClientOriginalExtension()));
            //Guardo la direccion de la foto en la base de datos con el mismo nombre
            $user->foto = 'images/users/' . $random . '.' . ($file->getClientOriginalExtension());
        }else{
          $user->foto = 'images/users/default.png';
        }

        $user->save();

        return redirect(route('user.list'));
    }

    public function destroy(Request $request)
    {
        $user = User::findOrFail($request->id);
        $this->removeImage($user->foto);
        $user->delete($request->id);
        return redirect(route('user.list'));
    }
    public function removeImage($url)
    {
        if (\File::exists(public_path($url)) && public_path($url) != public_path('images/users/default.png')) {
            \File::delete(public_path($url));
        }
      }

}
