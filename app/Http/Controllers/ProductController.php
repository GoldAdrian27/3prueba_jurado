<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\FotosDeProducto;
use App\Models\Category;
use Illuminate\Http\Request;
use App\http\Requests\ProductRequest;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::All();
        return view('product.list', compact('products'));
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('product.show', compact('product'));
    }

    public function create()
    {
        $categories = Category::All();
        return view('product.create', compact('categories'));
    }

    public function store(ProductRequest $request)
    {
        $product = new Product();
        $product->nombre = request('nombre');

        //Si contiene una foto
        if ($request->hasFile('foto')) {
            $file = $request->foto;
            //Cambio el nombre a la foto con un string aleatorio y la guardo en una carpeta en public
            $random = Str::random(20);
            //Si existe esa imagen le cambio el nombre
            while (is_file(public_path() . '/images/products/' . $random . '.' . $file->getClientOriginalExtension())) {
                $random = Str::random(20);
            }
            $file->move(public_path() . '/images/products/', $random . '.' . ($file->getClientOriginalExtension()));
            //Guardo la direccion de la foto en la base de datos con el mismo nombre
            $product->foto = 'images/products/' . $random . '.' . ($file->getClientOriginalExtension());
        } else {
            $product->foto = 'images/products/default.png';
        }

        //Galeria de fotos----------------------------------------------

        //Si contiene las fotos de la galeria
        if ($request->hasFile('fotos')) {
            $urlsFotos;
            $files = $request->fotos;
            //Cambio el nombre a la foto con un string aleatorio y la guardo en una carpeta en public
            $i = 0;
            foreach ($files as $file) {
                $random = Str::random(20);
                //Si existe esa imagen le cambio el nombre
                while (is_file(public_path() . '/images/products/galeria/' . $random . '.' . $file->getClientOriginalExtension())) {
                    $random = Str::random(20);
                }
                $file->move(public_path() . '/images/products/galeria/', $random . '.' . ($file->getClientOriginalExtension()));


                //Guardo la direccion de la foto en la base de datos con el mismo nombre
                $urlsFotos[$i++] = 'images/products/galeria/' . $random . '.' . ($file->getClientOriginalExtension());
            }
        }
        //-------------------------------------------------------Galeria de fotos
        $product->descripcion = request('descripcion');
        $product->save();

        //Galeria de fotos----------------------------------------------
        if ($request->hasFile('fotos')) {
            //Guardando las fotos en la tabla de Fotos_de_producto
            foreach ($urlsFotos as $url) {
                $fotosProducto = new FotosDeProducto();
                $fotosProducto->url = $url;
                $fotosProducto->product_id = $product->id;
                $fotosProducto->save();
            }
        }
        //-------------------------------------------------------Galeria de fotos

        //Relaciono todas las categorias pasadas por parametro con este producto
        $product->categories()->sync($request->categorias);
        $product->save();

        return redirect(route('product.list'));
    }

    public function edit($id)
    {
        $product = Product::FindOrFail($id);
        $categories = Category::All();

        return view('product.edit', compact('product', 'categories'));
    }

    public function update(ProductRequest $request)
    {
        $product = Product::FindOrFail(request('id'));

        $product->nombre = request('nombre');
        $product->descripcion = request('descripcion');
        $product->categories()->sync($request->categorias);

        //Si contiene una foto
        if ($request->hasFile('foto')) {
          $this->removeImage($product->foto);
            $file = $request->foto;
            //Cambio el nombre a la foto con un string aleatorio y la guardo en una carpeta en public
            $random = Str::random(20);
            //Si existe esa imagen le cambio el nombre
            while (is_file(public_path() . '/images/products/' . $random . '.' . $file->getClientOriginalExtension())) {
                $random = Str::random(20);
            }
            $file->move(public_path() . '/images/products', $random . '.' . ($file->getClientOriginalExtension()));
            //Guardo la direccion de la foto en la base de datos con el mismo nombre
            $product->foto = 'images/products/' . $random . '.' . ($file->getClientOriginalExtension());
        } else {
            $product->foto = 'images/products/default.png';
        }
        $product->save();

        //Si contiene las fotos de la galeria
        if ($request->hasFile('fotos')) {
            $urlsFotos;
            $files = $request->fotos;
            //Cambio el nombre a la foto con un string aleatorio y la guardo en una carpeta en public
            $i = 0;

            foreach ($files as $file) {
                $random = Str::random(20);
                //Si existe esa imagen le cambio el nombre
                while (is_file(public_path() . '/images/products/galeria/' . $product->id . '/' . $random . '.' . $file->getClientOriginalExtension())) {
                    $random = Str::random(20);
                }
                $file->move(public_path() . '/images/products/galeria/' . $product->id . '/', $random . '.' . ($file->getClientOriginalExtension()));


                //Guardo la direccion de la foto en la base de datos con el mismo nombre
                $urlsFotos[$i++] = 'images/products/galeria/' . $product->id . '/' . $random . '.' . ($file->getClientOriginalExtension());
            }

            //Guardando las fotos en la tabla de Fotos_de_producto
            foreach ($urlsFotos as $url) {
                $fotosProducto = new FotosDeProducto();
                $fotosProducto->url = $url;
                $fotosProducto->product_id = $product->id;
                $fotosProducto->save();
            }
        }

          return redirect()->back();
    }

    public function destroy(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $this->removeImage($product->foto);
        foreach ($product->fotos as $foto) {
          $this->removeImage($foto->url);
        }
        if (\File::exists(public_path('images/products/galeria/'. $product->id))) {
          \File::deleteDirectory('images/products/galeria/'. $product->id);
        }
        $product->delete($request->id);
        return redirect(route('product.list'));
    }

    public function removeImage($url)
    {
        if (\File::exists(public_path($url)) && public_path($url) != public_path('images/products/default.png')) {
            \File::delete(public_path($url));
        }
    }
}
