<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\http\Requests\CategoryRequest;

class CategoryController extends Controller
{
  public function index() {
    $categories = Category::All();
    return view('category.list', compact('categories'));
  }

  public function show($id) {
    $category = Category::findOrFail($id);
    return view('category.show', compact('category'));
  }

  public function create() {
    return view('category.create');
  }

  public function store(CategoryRequest $request) {
    $category = new Category();

    $category->nombre = request('nombre');
    $category->descripcion = request('descripcion');
    $category->save();

    return redirect(route('category.list'));
  }

  public function edit($id) {
    $category = Category::FindOrFail($id);

    return view('category.edit', compact('category'));
  }

  public function update(CategoryRequest $request) {
    $category = Category::FindOrFail(request('id'));

    $category->nombre = request('nombre');
    $category->descripcion = request('descripcion');

    $category->save();

    return redirect(route('category.list'));
  }

  public function destroy(Request $request) {
    $category = Category::findOrFail($request->id);
    $category->delete($request->id);
    return redirect(route('category.list'));
  }
}
