<?php

namespace App\Http\Controllers;

use App\Models\Tarifas;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\TarifasRequest;
class TarifasController extends Controller
{
  public function index() {
    $tarifas = Tarifas::All();
    return view('tarifas.list', compact('tarifas'));
  }

  public function show($id) {
    $tarifa = Tarifas::findOrFail($id);
    return view('tarifas.show', compact('tarifa'));
  }

  public function create() {
    $products = Product::All();
    return view('tarifas.create', compact('products'));
  }

  public function store(TarifasRequest $request) {
    $tarifas = new Tarifas();

    $tarifas->product_id = request('product_id');
    $tarifas->fecha_inicial = request('fecha_inicial');
    $tarifas->fecha_final = request('fecha_final');
    $tarifas->precio = request('precio');
    $tarifas->save();

    return redirect(route('tarifa.list'));
  }

  public function edit($id) {
    $tarifa = Tarifas::FindOrFail($id);
    $products = Product::All();
    return view('tarifas.edit', compact('tarifa', 'products'));
  }

  public function update(TarifasRequest $request) {
    $tarifas = Tarifas::FindOrFail(request('id'));
    $tarifas->product_id = request('product_id');
    $tarifas->fecha_inicial = request('fecha_inicial');
    $tarifas->fecha_final = request('fecha_final');
    $tarifas->precio = request('precio');
    $tarifas->save();

    $tarifas->save();

    return redirect(route('tarifa.list'));
  }

  public function destroy(Request $request) {
    $tarifas = Tarifas::findOrFail($request->id);
    $tarifas->delete($request->id);
    return redirect(route('tarifa.list'));
  }
}
