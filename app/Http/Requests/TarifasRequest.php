<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TarifasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
          'fecha_inicial' => 'required|date',
          'fecha_final' => 'required|date',
          'precio' => 'required|numeric',
        ];
    }

    public function messages(){
      return[
        'fecha_inicial.required' => 'El campo fecha inicio esta incompleto',
        'fecha_inicial.date' => 'El campo fecha final tiene que ser una fecha',
        'fecha_final.required' => 'El campo fecha final esta incompleto',
        'fecha_final.date' => 'El campo fecha final tiene que ser una fecha',
        'precio.required' => 'El campo precio esta incompleto',
        'precio.numeric' => 'El campo precio tiene que ser un numero',
      ];
    }

}
