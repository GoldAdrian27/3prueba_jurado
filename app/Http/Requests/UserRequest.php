<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

          'nombre' => 'required',
          'apellido' => 'required',
          'fecha_nacimiento' => 'required',
          'email' => [
                  'required',
                  Rule::unique('users')->ignore(request('id')),
              ],
          'password' => 'required|min:8',
          'foto' => 'mimes:jpeg,jpg,png'

        ];
    }
    public function messages(){
      return[
        'nombre.required' => 'El campo nombre esta incompleto',
        'apellido.required' => 'El campo apellido esta incompleto',
        'email.required' => 'El campo email esta incompleto',
        'email.unique' => 'El campo email ya esta en uso',
        'fecha_nacimiento.required' => 'El campo fecha esta incompleto',
        'password.required' => 'El campo contraseña esta incompleto',
        'password.min' => 'La contraseña tiene que ser mayor a 8 caracteres',
        'foto.mimes' => 'En el campo de foto de perfil solo se aceptan formatos jpeg, jpg y png',

      ];
    }
}
