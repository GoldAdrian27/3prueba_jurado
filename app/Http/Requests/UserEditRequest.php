<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //si la contraseña no esta vacia validala
        if (!empty(request('password'))) {
            return [
              'password' => 'min:8',
              'fecha_nacimiento' => 'date',
              'foto' => 'mimes:jpeg,jpg,png',
              'email' => [
                      'required',
                      'email',
                      Rule::unique('users')->ignore(request('id')),
                  ],            ];
        } else {
            return [
            'fecha_nacimiento' => 'date',
            'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->ignore(request('id')),
                    ],
              ];
        }
    }


    public function messages()
    {
        return[
          'password.min' => 'La contraseña tiene que ser mayor a 8 caracteres',
          'email.email' => 'El campo de email tiene que ser una direccion EMAIL',
          'foto.mimes' => 'En el campo de foto de perfil solo se aceptan formatos jpeg, jpg y png',
          'fecha_nacimiento' => 'El campo fecha de nacimiento tiene que ser una FECHA'
        ];
    }
}
