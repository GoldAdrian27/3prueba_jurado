<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
           'nombre' => 'required|min:3',
           'descripcion' => 'required',
           'foto' => 'mimes:jpeg,jpg,png|max:5000',
           'fotos.*' => 'mimes:jpeg,jpg,png|max:10000',
         ];
     }

     public function messages(){
       return[
         'nombre.required' => 'El campo nombre esta incompleto',
         'descripcion.required' => 'El campo descripcion esta incompleto',
         'foto.mimes' => 'En el campo de foto de perfil solo se aceptan formatos jpeg, jpg y png',
         'fotos.*.mimes' => 'En el campo de fotos de la galeria solo se aceptan formatos jpeg, jpg y png',
         'foto.max' => 'En el campo de foto de perfil solo se aceptan tamaños inferiores a 5MB',
         'fotos.*.max' => 'En el campo de fotos de la galeria solo se aceptan tamaños inferiores a 10MB',

       ];
     }
}
