<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'email' => 'required|email',
           'password' => 'required|min:8',
         ];
    }
    public function messages()
    {
        return[
          'email.required' => 'El campo email esta incompleto',
          'email.email' => 'El campo email no es un email',
         'password.required' => 'El campo contraseña esta incompleto',
         'password.min' => 'La contraseña tiene que ser mayor a 8 caracteres'
       ];
    }
}
