<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TarifasController;
use App\Http\Controllers\FotosDeProductoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('index');
})->middleware('auth')->name('index');

//--------Log in------------
Route::middleware(['guest'])->group(function(){
  Route::get('/login', function () { return view('user.login'); })->name('login');
  Route::POST('/user/login', [UserController::class, 'login'])->name('user.login');
  Route::get('/register', function () { return view('user.register'); })->name('register.form');
  Route::POST('/user/register', [UserController::class, 'register'])->name('user.register');
});

Route::get('/user/logout', [UserController::class, 'logout'])->name('user.logout');

//Todas estas rutas solo se pueden usar si el usuario ha iniciado sesion
//------CRUD------------
Route::middleware(['auth'])->group(function(){
  //Users----------------------------
  Route::Get('user/list', [UserController::class, 'index'])->name('user.list');
  Route::Get('user/show/{id}', [UserController::class, 'show'])->name('user.show');
  Route::Get('user/create', [UserController::class, 'create'])->name('user.create');
  Route::Post('user/store', [UserController::class, 'store'])->name('user.store');
  Route::Get('user/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
  Route::Post('user/update', [UserController::class, 'update'])->name('user.update');
  Route::Post('user/destroy/', [UserController::class, 'destroy'])->name('user.destroy');

  //Category----------------------------
  Route::Get('category/list', [CategoryController::class, 'index'])->name('category.list');
  Route::Get('category/show/{id}', [CategoryController::class, 'show'])->name('category.show');
  Route::Get('category/create', [CategoryController::class, 'create'])->name('category.create');
  Route::Post('category/store', [CategoryController::class, 'store'])->name('category.store');
  Route::Get('category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
  Route::Post('category/update', [CategoryController::class, 'update'])->name('category.update');
  Route::Post('category/destroy/', [CategoryController::class, 'destroy'])->name('category.destroy');

  //Products----------------------------
  Route::Get('product/list', [ProductController::class, 'index'])->name('product.list');
  Route::Get('product/show/{id}', [ProductController::class, 'show'])->name('product.show');
  Route::Get('product/create', [ProductController::class, 'create'])->name('product.create');
  Route::Post('product/store', [ProductController::class, 'store'])->name('product.store');
  Route::Get('product/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
  Route::Post('product/update', [ProductController::class, 'update'])->name('product.update');
  Route::Post('product/destroy/', [ProductController::class, 'destroy'])->name('product.destroy');

  //Tarifas----------------------------
  Route::Get('tarifas/list', [TarifasController::class, 'index'])->name('tarifa.list');
  Route::Get('tarifas/show/{id}', [TarifasController::class, 'show'])->name('tarifa.show');
  Route::Get('tarifas/create', [TarifasController::class, 'create'])->name('tarifa.create');
  Route::Post('tarifas/store', [TarifasController::class, 'store'])->name('tarifa.store');
  Route::Get('tarifas/edit/{id}', [TarifasController::class, 'edit'])->name('tarifa.edit');
  Route::Post('tarifas/update', [TarifasController::class, 'update'])->name('tarifa.update');
  Route::Post('tarifas/destroy/', [TarifasController::class, 'destroy'])->name('tarifa.destroy');


  //FotosDeProducto
  Route::Post('fotos-de-producto/destroy/', [FotosDeProductoController::class, 'destroy'])->name('fotos-de-producto.destroy');
  Route::Post('fotos-de-producto/destroyall/', [FotosDeProductoController::class, 'destroyAll'])->name('fotos-de-producto.destroyAll');
});
