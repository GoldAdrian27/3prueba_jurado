<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Category;
use App\Models\Product;
use App\Models\Tarifas;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/categories', function (Request $request) {
  $category = Category::All();
  return response()->json($category);
});


Route::middleware('auth:api')->get('/productos', function (Request $request) {
  $products = Product::All();
  //por cada producto le añado todos los precios de las tarifas que tiene asociadas y sus categorias
  foreach ($products as $product) {
    $product->tarifas = DB::table('products')->leftJoin('tarifas', 'products.id', '=', 'tarifas.product_id')
                          ->where('tarifas.fecha_inicial', '<', now())
                          ->where('tarifas.fecha_final', '>', now())
                          ->where('tarifas.product_id', '=', $product->id)
                          ->select('tarifas.precio')
                          ->get();

    $product->categories = $product->categories;
  }
  return response()->json($products);

});

//   $products = Tarifas::Where('fecha_inicial', '<', now())->where('fecha_final', '<', now())->get();
//
//   foreach ($products as $product) {
//     $product->product_id = Product::findorfail($product->product_id);
//   }
//
//   return response()->json($products);
// });
