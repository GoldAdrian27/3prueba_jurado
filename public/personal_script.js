//Comprobar en que pagina estoy
var page = $('.nav-treeview .nav-item .nav-link[href="' + window.location.href + '"]:first');
//Añadir la clase active para que resalte en la barra de navegacion
page.addClass("active")

//Comprueba que la fecha final es menor a la fecha inicial
$("#id_fecha_inicial").on("change", function() {
    var inicial = $(this).val();

    $("#id_fecha_final").attr('min', inicial);

    if (Date.parse(inicial) >= Date.parse($("#id_fecha_final").val())) {
        $("#id_fecha_final").val('');
    }
});



//Menu dropdown

$('#show-logged-options').on('click', function() {
    $('#logged-options').slideDown();



});

$(document).mouseup(function(e) {
    var container = $("#logged-options");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.slideUp();
    }
});


//imagenes ampliable

$('.imagenPreview').on('click', function() {
    var idForPre = $(this).attr('idForPre');
    $("#imagenView-" + idForPre).slideDown();
});
//Si se pulsa fuera de la imagen se cerrará
$(document).mouseup(function(e) {
    var container = $(".imagenView");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.slideUp();
    }
});


//Formulario seleccionar todo
$('#seleccionarTodo').on('change', function() {
    //alert('hola');
    var checkboxes = $(this).closest('form').find(':checkbox');
    checkboxes.prop('checked', $(this).is(':checked'));
})


//Confirmacion eliminar galeria

$('#segundaOportunidadEliminar').on('click', function() {
    $("#eliminarGaleria").slideDown();
    $("#segundaOportunidadEliminar").prop("disabled", true);

});
//Si se pulsa fuera de la ventana emergente de confirmacion ésta se cerrará
$(document).mouseup(function(e) {
    var container = $("#eliminarGaleria");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.slideUp();
        $("#segundaOportunidadEliminar").prop("disabled", false);
    }
});

$('#noEliminarGaleria').on('click', function() {
    $("#eliminarGaleria").slideUp();
    $("#segundaOportunidadEliminar").prop("disabled", false);
});

//Confirmacion eliminar usuarioLog

$('#segundaOportunidadEliminarUsuarioLog').on('click', function() {
    $("#eliminarUsuarioLog").slideDown();
    $("#segundaOportunidadEliminarUsuarioLog").prop("disabled", true);

});

//Si se pulsa fuera de la ventana emergente de confirmacion ésta se cerrará
$(document).mouseup(function(e) {
    var container = $("#eliminarUsuarioLog");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.slideUp();
        $("#segundaOportunidadEliminarUsuarioLog").prop("disabled", false);
    }
});

$('#noEliminarUsuarioLog').on('click', function() {
    $("#eliminarUsuarioLog").slideUp();
    $("#segundaOportunidadEliminarUsuarioLog").prop("disabled", false);
});




// Custom Scripts

function googleTranslateElementInit() {
    new google.translate.TranslateElement({
        pageLanguage: 'es'
    }, 'google_translate_element');
}


// Minified Scripts
(function() {
    var gtConstEvalStartTime = new Date();
    /*

     Copyright The Closure Library Authors.
     SPDX-License-Identifier: Apache-2.0
    */
    function d(b) {
        var a = document.getElementsByTagName("head")[0];
        a || (a = document.body.parentNode.appendChild(document.createElement("head")));
        a.appendChild(b)
    }

    function _loadJs(b) {
        var a = document.createElement("script");
        a.type = "text/javascript";
        a.charset = "UTF-8";
        a.src = b;
        d(a)
    }

    function _loadCss(b) {
        var a = document.createElement("link");
        a.type = "text/css";
        a.rel = "stylesheet";
        a.charset = "UTF-8";
        a.href = b;
        d(a)
    }

    function _isNS(b) {
        b = b.split(".");
        for (var a = window, c = 0; c < b.length; ++c)
            if (!(a = a[b[c]])) return !1;
        return !0
    }

    function _setupNS(b) {
        b = b.split(".");
        for (var a = window, c = 0; c < b.length; ++c) a.hasOwnProperty ? a.hasOwnProperty(b[c]) ? a = a[b[c]] : a = a[b[c]] = {} : a = a[b[c]] || (a[b[c]] = {});
        return a
    }
    window.addEventListener && "undefined" == typeof document.readyState && window.addEventListener("DOMContentLoaded", function() {
        document.readyState = "complete"
    }, !1);
    if (_isNS('google.translate.Element')) {
        return
    }(function() {
        var c = _setupNS('google.translate._const');
        c._cest = gtConstEvalStartTime;
        gtConstEvalStartTime = undefined;
        c._cl = 'en';
        c._cuc = 'googleTranslateElementInit';
        c._cac = '';
        c._cam = '';
        c._ctkk = '440335.1449305758';
        var h = 'translate.googleapis.com';
        var s = (true ? 'https' : window.location.protocol == 'https:' ? 'https' : 'http') + '://';
        var b = s + h;
        c._pah = h;
        c._pas = s;
        c._pbi = b + '/translate_static/img/te_bk.gif';
        c._pci = b + '/translate_static/img/te_ctrl3.gif';
        c._pli = b + '/translate_static/img/loading.gif';
        c._plla = h + '/translate_a/l';
        c._pmi = b + '/translate_static/img/mini_google.png';
        c._ps = b + '/translate_static/css/translateelement.css';
        c._puh = 'translate.google.com';
        _loadCss(c._ps);
        _loadJs(b + '/translate_static/js/element/main.js');
    })();
})();
