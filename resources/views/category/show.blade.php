@extends('layouts.main')
@section('title', 'Mostrando categoria: ' . $category->id)

@section('content')

<div style="margin:auto; width:40%">

    <div class="form-group row">
        <div class="card" style="width: 20rem; margin:auto">
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">{{$category->nombre}}</li>
                    <li class="list-group-item">{{$category->descripcion}}</li>


                </ul>
            </div>

            <div class="card-body">
                <a href="{{route('category.list')}}" class="btn btn-secondary float-right">Volver</a>
            </div>
        </div>

    </div>
</div>

@endsection
