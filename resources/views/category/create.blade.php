@extends('layouts.main')
@section('title', 'Crear nuevo usuario')

@section('content')

<div style="margin:auto; width:40%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form class="custom-forms" action="{{route('category.store')}}" method="POST">
        @csrf
        @method('POST')
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input type="text" name="nombre" value="{{old('nombre')}}" class="form-control" id="inputEmail3">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Descripcion</label>
            <div class="col-sm-10">
                <input type="text" name="descripcion" value="{{old('descripcion')}}" class="form-control" id="inputEmail3">
            </div>
        </div>

        <button type="submit" class="btn btn-success float-right">Crear</button>
        <a href="{{route('category.list')}}" class="btn btn-secondary float-left">Volver</a>

    </form>
</div>

@endsection
