@extends('layouts.main')
@section('title', 'Editando categoria: ' . $category->id)

@section('content')

<div style="margin:auto; width:40%">
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
    <form class="custom-forms" action="{{route('category.update')}}" method="POST">
        @csrf
        @method('POST')
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Id</label>
            <div class="col-sm-10">
                <input type="text" name="id" value="{{$category->id}}" class="form-control" id="inputEmail3" readOnly>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input type="text" name="nombre" value="{{$category->nombre}}" class="form-control" id="inputEmail3">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Descripcion</label>
            <div class="col-sm-10">
                <textarea type="text" name="descripcion" value="{{$category->descripcion}}" class="form-control" id="inputEmail3">{{$category->descripcion}}</textarea>
            </div>
        </div>

        <button type="submit" class="btn btn-success float-right">Modificar</button>
        <a href="{{route('category.list')}}" class="btn btn-secondary float-left">Volver</a>

    </form>
</div>

@endsection
