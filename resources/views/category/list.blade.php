@extends('layouts.main')
@section('title', 'Lista de categoria')
@section('content')
<div style="margin:auto; padding: 0 5%">
  <a href="{{route('category.create')}}" style="margin-bottom: 0.5%" class="btn btn-success float-right">Nuevo</a>
    <table id="dataTables">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
            <tr>
                <th scope="row">{{$category->id}}</th>
                <td>{{$category->nombre}}</td>
                <td>{{$category->descripcion}}</td>
                <td>
                  <form class="" action="{{route('category.destroy')}}" method="POST">
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <a href="{{route('category.show', $category->id)}}" class="btn btn-secondary">Ver</a>
                      <a href="{{route('category.edit', $category->id)}}" class="btn btn-secondary">Editar</a>

                      @csrf
                      <input type="text" name="id" value="{{$category->id}}" hidden>
                      <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>

                  </form>

                </td>

            </tr>
            @endforeach

        </tbody>
    </table>

</div>
@endsection
