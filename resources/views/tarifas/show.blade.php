@extends('layouts.main')
@section('title', 'Mostrando tarifa: ' . $tarifa->id)

@section('content')

<div style="margin:auto; ">
    <div class="card" style="width: 20rem; margin:auto">
        <div class="card-body">
            <h5 class="card-title">Nº: {{$tarifa->id}}</h5>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Fecha Inicial: {{$tarifa->fecha_inicial}}</li>
          <li class="list-group-item">Fecha final: {{$tarifa->fecha_final}}</li>
          <li class="list-group-item">Precio: {{$tarifa->precio}}€</li>
          <li class="list-group-item">Producto: {{$tarifa->product->nombre}}</li>


        </ul>
      </div>

        <div class="card-body">
            <a href="#" class="card-link"></a>
            <a href="{{route('tarifa.list')}}" class="btn btn-secondary float-right">Volver</a>
        </div>
    </div>

</div>

@endsection
