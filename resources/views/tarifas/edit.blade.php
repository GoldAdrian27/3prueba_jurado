@extends('layouts.main')
@section('title', 'Editando tarifa: ' . $tarifa->id)

@section('content')

<div style="margin:auto; width:40%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form class="custom-forms form-tarifas" action="{{route('tarifa.update')}}" method="POST">
      @csrf
      @method('POST')
      <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Id</label>
          <div class="col-sm-10">
              <input readonly type="text" name="id" value="{{$tarifa->id}}" class="form-control" id="">
          </div>
      </div>
      <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Fecha Inicial</label>
          <div class="col-sm-10">
              <input type="date" name="fecha_inicial" value="{{$tarifa->fecha_inicial}}" class="form-control" id="id_fecha_inicial">
          </div>
      </div>
      <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Fecha Final</label>
          <div class="col-sm-10">
              <input type="date" name="fecha_final" value="{{$tarifa->fecha_final}}" class="form-control" id="id_fecha_final" min="{{$tarifa->fecha_inicial}}">
          </div>
      </div>

      <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Precio</label>
          <div class="col-sm-10">
              <input type="number" step="0.01" name="precio" value="{{$tarifa->precio}}" class="form-control" id="">
          </div>
      </div>

      <div class="form-group">
        <label for="exampleFormControlSelect1">Producto</label>
        <select class="form-control" name="product_id" id="exampleFormControlSelect1">
          @foreach ($products as $product)
            <option value="{{$product->id}}">{{$product->nombre}}</option>
          @endforeach
        </select>

        </div>
        <button type="submit" class="btn btn-success float-right">Modificar</button>
        <a href="{{route('tarifa.list')}}" class="btn btn-secondary float-left">Volver</a>

        {{-- <div class="form-group row">
    <div class="col-sm-10">
      <input type="date" class="form-control" id="inputDate3" >
    </div>
  </div> --}}
    </form>
</div>

@endsection
