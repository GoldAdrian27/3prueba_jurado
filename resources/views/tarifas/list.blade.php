@extends('layouts.main')
@section('title', 'Lista de tarifas')
@section('content')
<div style="margin:auto; padding: 0 5%">
    <a href="{{route('tarifa.create')}}" style="margin-bottom: 0.5%" class="btn btn-success float-right">Nuevo</a>
    <table id="dataTables">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Fecha Inicial</th>
                <th scope="col">Fecha Final</th>
                <th scope="col">Precio</th>
                <th scope="col">Producto Relacionado</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($tarifas as $tarifa)
            <tr>
                <th scope="row">{{$tarifa->id}}</th>
                <td>{{$tarifa->fecha_inicial}}</td>
                <td>{{$tarifa->fecha_final}}</td>
                <td>{{$tarifa->precio}}€</td>
                <td>
                  @if(!empty($tarifa->product))
                  {{$tarifa->product->nombre}}
                  @endif
                </td>
                <td>
                    <form class="" action="{{route('tarifa.destroy')}}" method="POST">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{route('tarifa.show', $tarifa->id)}}" class="btn btn-secondary">Ver</a>
                            <a href="{{route('tarifa.edit', $tarifa->id)}}" class="btn btn-secondary">Editar</a>

                            @csrf
                            <input type="text" name="id" value="{{$tarifa->id}}" hidden>
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </div>

                    </form>

                </td>

            </tr>
            @endforeach

        </tbody>
    </table>

</div>
@endsection
