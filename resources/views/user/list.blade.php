@extends('layouts.main')
@section('title', 'Lista de usuarios')
@section('content')
<div style="margin:auto; padding: 0 5%">
    <a href="{{route('user.create')}}" style="margin-bottom: 0.5%" class="btn btn-success float-right">Nuevo</a>
    <table id="dataTables">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Foto</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Email</th>
                <th scope="col">Fecha de nacimiento</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            @if ($user != auth()->user())

            <tr>
                <th scope="row">{{$user->id}}</th>
                <td><img src="{{asset($user->foto)}}" width="50px" alt=""></td>
                <td>{{$user->nombre}}</td>
                <td>{{$user->apellido}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->fecha_nacimiento}}</td>
                <td>
                    <form class="" action="{{route('user.destroy')}}" method="POST">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{route('user.show', $user->id)}}" class="btn btn-secondary">Ver</a>
                            <a href="{{route('user.edit', $user->id)}}" class="btn btn-secondary">Editar</a>

                            @csrf
                            <input type="text" name="id" value="{{$user->id}}" hidden>
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </div>

                    </form>

                </td>

            </tr>
            @endif
            @endforeach

        </tbody>
    </table>

</div>
@endsection
