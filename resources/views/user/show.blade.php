@extends('layouts.main')
@section('title', 'Mostrando usuario: ' . $user->id)

@section('content')

<div style="margin:auto; width:40%">

  <div style="margin:auto; ">
      <div class="card" style="width: 18rem; margin:auto">
          <img src="{{asset($user->foto)}}" width="" class="card-img-top" alt="">
          <div class="card-body">
              <h5 class="card-title">{{$user->nombre}}</h5>
              <h6 class="card-text">{{$user->apellido}}</h6>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">{{$user->email}}</li>
            <li class="list-group-item">{{$user->fecha_nacimiento}}</li>
          </ul>
          <div class="card-body">
              <a href="{{route('user.list')}}" class="btn btn-secondary float-right">Volver</a>
          </div>
      </div>

  </div>

@endsection
