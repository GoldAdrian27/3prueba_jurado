@extends('layouts.main')
@section('content')

<div style="margin:auto; width:40%">
    <div class="card">
        <div class="card-header">
            Registrarse
        </div>
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                @if ($errors->any())
                <div class="alert alert-danger" style="padding: 5px">
                    <ul style="font-size:12px; margin: 0">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form class="custom-forms" enctype="multipart/form-data" class="mx-auto" action="{{route('user.register')}}" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <input type="text" name="nombre" value="Adrian" class="form-control" placeholder="Nombre">
                        </div>
                        <div class="form-group col-md-5">
                            <input type="text" name="apellido" value="Jurado" class="form-control" placeholder="Apellido">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input type="email" name="email" value="adrian@ejemplo.com" class="form-control" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input type="password" name="password" value="123456789" class="form-control" id="inputPassword3" placeholder="Contraseña">
                        </div>
                    </div>
                    <span style="font-size:12px">Fecha de nacimiento</span>
                    <div class="form-group row">

                        <div class="col-sm-10">
                            <input type="date" max={{now()}} name="fecha_nacimiento" value="" class="form-control" id="inputEmail3">
                        </div>
                    </div>

                    <span style="font-size:12px">Imagen de perfil</span>
                    <div class="form-group">
                        <input type="file" name="foto" class="form-control-file">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Registrarse</button>
                        </div>
                    </div>
                </form>
                <footer class="blockquote-footer">Ya tienes cuenta? <cite title="Source Title"> <a href="{{route('login')}}">Inicia sesión</a> </cite></footer>
            </blockquote>
        </div>
    </div>
</div>

@endsection
