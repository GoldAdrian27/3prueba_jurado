@extends('layouts.main')
@section('content')
<div style="margin:auto; width:40%">

    <div class="card">
        <div class="card-header">
            Iniciar sesión
        </div>
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                @if ($errors->any())
                <div class="alert alert-danger" style="padding: 5px">
                    <ul style="font-size:12px; margin: 0">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{route('user.login')}}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input type="email" value="adrian@ejemplo.com" name="email" class="form-control" id="inputEmail3" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Contraseña">
                        </div>
                    </div>
                    {{-- <fieldset class="form-group">
          <div class="row">
            <legend class="col-form-label col-sm-2 pt-0">Radios</legend>
            <div class="col-sm-10">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                <label class="form-check-label" for="gridRadios1">
                  First radio
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                <label class="form-check-label" for="gridRadios2">
                  Second radio
                </label>
              </div>
              <div class="form-check disabled">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" disabled>
                <label class="form-check-label" for="gridRadios3">
                  Third disabled radio
                </label>
              </div>
            </div>
          </div>
        </fieldset> --}}
                    {{-- <div class="form-group row">
          <div class="col-sm-2">Checkbox</div>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="gridCheck1">
              <label class="form-check-label" for="gridCheck1">
                Example checkbox
              </label>
            </div>
          </div>
        </div> --}}
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Iniciar sesión</button>
                        </div>
                    </div>
                </form>
                <footer class="blockquote-footer">No tienes cuenta? <cite title="Source Title"> <a href="{{route('register.form')}}">Registrate</a> </cite></footer>
            </blockquote>
        </div>
    </div>


</div>

@endsection
