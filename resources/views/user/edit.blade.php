@extends('layouts.main')
@section('title', 'Editando usuario: ' . $user->id)

@section('content')

<div style="margin:auto; width:40%">
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
    <form class="custom-form" enctype="multipart/form-data" action="{{route('user.update')}}" method="POST">
        @csrf
        @method('POST')
        <div class="form-group row">
            <label for="inputId" class="col-sm-2 col-form-label">Id</label>
            <div class="col-sm-10">
                <input type="text" name="id" value="{{$user->id}}" class="form-control" id="inputId" readOnly>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputNombre" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input type="text" name="nombre" value="{{$user->nombre}}" class="form-control" id="inputNombre">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputApellido" class="col-sm-2 col-form-label">Apellido</label>
            <div class="col-sm-10">
                <input type="text" name="apellido" value="{{$user->apellido}}" class="form-control" id="inputApellido">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" name="email" value="{{$user->email}}" class="form-control" id="inputEmail">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Contraseña</label>
            <div class="col-sm-10">
                <input type="password" name="password" value="" class="form-control" id="inputEmail3">
            </div>
        </div>

        <div class="form-group row">
            <label for="InputDate"  class="col-sm-2 col-form-label">Fecha de nacimiento</label>
            <div class="col-sm-10">
                <input type="date" max={{now()}} name="fecha_nacimiento" value="{{$user->fecha_nacimiento}}" class="form-control" id="InputDate">
            </div>
        </div>
        <span style="font-size:12px">Imagen de perfil</span>
        <div class="form-group">
            <input type="file" name="foto" class="form-control-file">
        </div>

        <button type="submit" class="btn btn-success float-right">Modificar</button>
        <a href="{{route('user.list')}}" class="btn btn-secondary float-left">Volver</a>

        {{-- <div class="form-group row">
    <div class="col-sm-10">
      <input type="date" class="form-control" id="inputDate3" >
    </div>
  </div> --}}
    </form>
</div>

@endsection
