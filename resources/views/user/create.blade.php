@extends('layouts.main')
@section('title', 'Crear nuevo usuario')

@section('content')

<div style="margin:auto; width:40%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form class="custom-form" enctype="multipart/form-data" action="{{route('user.store')}}" method="POST">
        @csrf
        @method('POST')
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input type="text" name="nombre" value="{{old('nombre')}}" class="form-control" id="inputEmail3">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Apellido</label>
            <div class="col-sm-10">
                <input type="text" name="apellido" value="{{old('apellido')}}" class="form-control" id="inputEmail3">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" name="email" value="{{old('email')}}" class="form-control" id="inputEmail3">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Contraseña</label>
            <div class="col-sm-10">
                <input type="password" name="password" value="" class="form-control" id="inputEmail3">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Fecha de nacimiento</label>
            <div class="col-sm-10">
                <input type="date" max={{now()}} name="fecha_nacimiento" value="" class="form-control" id="inputEmail3">
            </div>
        </div>
        <span style="font-size:12px">Imagen de perfil</span>
        <div class="form-group">
            <input type="file" name="foto" class="form-control-file">
        </div>
        <button type="submit" class="btn btn-success float-right">Crear</button>
        <a href="{{route('user.list')}}" class="btn btn-secondary float-left">Volver</a>

        {{-- <div class="form-group row">
    <div class="col-sm-10">
      <input type="date" class="form-control" id="inputDate3" >
    </div>
  </div> --}}
    </form>
</div>

@endsection
