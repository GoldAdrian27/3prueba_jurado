<!DOCTYPE html>
<html>
@include('layouts.head')
<body class="hold-transition sidebar-mini layout-fixed">
  @yield('content')



<!-- ./wrapper -->
@include('layouts.scripts')

</body>
</html>
