@extends('layouts.main')
@section('title', 'Editando producto: ' . $product->id)

@section('content')
<div style="margin:auto; width:40%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form enctype="multipart/form-data" name="form" class="custom-forms" action="{{route('product.update')}}" method="POST">
        @csrf
        @method('POST')
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Id</label>
            <div class="col-sm-10">
                <input type="text" name="id" value="{{$product->id}}" class="form-control" id="inputEmail3" readOnly>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input type="text" name="nombre" value="{{$product->nombre}}" class="form-control" id="inputEmail3">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Descripcion</label>
            <div class="col-sm-10">
                <input type="text" name="descripcion" value="{{$product->descripcion}}" class="form-control" id="inputEmail3">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Categorias</label>
            <div class="" style="margin: 30px 0">
              <input type="checkbox" class="form-check-input" id="seleccionarTodo" value="Seleccionar Todo">
              <label class="form-check-label" for="seleccionarTodo"> Seleccionar Todo</label>
            </div>
            <div class="form-check column-category">

                @foreach ($categories as $catGeneral)
                @if($product->hasCategory($catGeneral->id))
                    <div class="form-check form-check-inline">
                        <input checked type="checkbox" name="categorias[]" class="form-check-input" id="inlineCheckbox{{$catGeneral->id}}" value="{{$catGeneral->id}}">
                        <label class="form-check-label" for="inlineCheckbox{{$catGeneral->id}}">{{$catGeneral->nombre}}</label>
                    </div>
                    @else
                    <div class="form-check form-check">

                        <input type="checkbox" name="categorias[]" class="form-check-input" id="inlineCheckbox{{$catGeneral->id}}" value="{{$catGeneral->id}}">
                        <label class="form-check-label" for="inlineCheckbox{{$catGeneral->id}}">{{$catGeneral->nombre}}</label>
                    </div>

                    @endif
                    @endforeach
            </div>
        </div>

        <span style="font-size:12px">Imagen de perfil</span>
        <div class="form-group">
            <input type="file" name="foto" class="form-control-file">
        </div>

        <span style="font-size:12px">Galeria de fotos</span>
        <div class="form-group">
            <input type="file" name="fotos[]" class="form-control-file" multiple>
        </div>


        <button type="submit" name="form" class="btn btn-success float-right">Modificar</button>
        <a href="{{route('product.list')}}" class="btn btn-secondary float-left">Volver</a>
    </form>

    {{-- Galeria --}}
    <div class="card-body">
      <h4>Modificar la galeria</h4>
        <ul class="list-group list-group-flush">
            <li class="list-group-item font-weight-bold">Galeria: </li>
            @if (count($product->fotos) == 0)
            <li class="list-group-item">No tiene fotos</li>
          @else

            {{-- Formulario para la confirmacion de eliminación --}}
            <button class="btn btn-danger" id="segundaOportunidadEliminar">Eliminar Todo</button>
            <div class="confirmacionEliminacion" id="eliminarGaleria">
              <p>¿Estas seguro que quieres eliminar toda la galeria?</p>
              <form  action="{{route('fotos-de-producto.destroyAll')}}" name="form2" method="post">
                @csrf
                @method('post')
                <input hidden type="text" name="id" value="{{$product->id}}">
                  <button type="submit" class="btn btn-danger">Eliminar Todo</button>
              </form>
              <a id="noEliminarGaleria" class="btn btn-secondary">Volver</a>
            </div>
            {{-- /Formulario para la confirmacion de eliminación --}}

            @foreach ($product->fotos as $foto)
            <li class="list-group-item" style="text-align:center">
                <form class="" action="{{route('fotos-de-producto.destroy')}}" name="form2" method="post">
                    @csrf
                    @method('post')
                    <input hidden type="text" name="id" value="{{$foto->id}}">
                    <img width="40%" class="imagenPreview" idForPre="{{$foto->id}}" id="imagenPreview-{{$foto->id}}" src="{{asset($foto->url)}}" alt="">
                    <button type="submit" class="btn btn-danger">X</button>
                    <img class="imagenView" id="imagenView-{{$foto->id}}" src="{{asset($foto->url)}}" alt="">
                </form>
            </li>
            @endforeach
          @endif

        </ul>
    </div>

    {{-- /Galeria --}}
</div>

@endsection
