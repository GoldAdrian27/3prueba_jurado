@extends('layouts.main')
@section('title', 'Mostrando producto: ' . $product->id)

@section('content')

<div style="margin:auto; ">
    <div class="card" style="width: 18rem; margin:auto">
        <img src="{{asset($product->foto)}}" width="" class="card-img-top" alt="">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">{{$product->nombre}}</h5>
            <p class="card-text">{{$product->descripcion}}</p>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item font-weight-bold">Categorias: </li>
            @if (count($product->categories) == 0)
              <li class="list-group-item">No tiene categoria</li>
            @endif
              @foreach ($product->categories as $categoy)
                <li class="list-group-item">{{$categoy->nombre}}</li>
              @endforeach

        </ul>

        <ul class="list-group list-group-flush">
          <li class="list-group-item font-weight-bold">Tarifas: </li>
            @if (count($product->tarifas) == 0)
              <li class="list-group-item">No tiene tarifas</li>
            @endif
              @foreach ($product->tarifas as $tarifa)
                @if($tarifa->fecha_inicial < now() && $tarifa->fecha_final > now())
                  <li class="list-group-item">ID-{{$tarifa->id}} : {{$tarifa->precio . '€ '}}</li>
                @endif
              @endforeach
        </ul>

        {{-- Galeria --}}
            <ul class="list-group list-group-flush">
                <li class="list-group-item font-weight-bold">Galeria: </li>
                @if (count($product->fotos) == 0)
                <li class="list-group-item">No tiene fotos</li>
                @endif
                @foreach ($product->fotos as $foto)
                <li class="list-group-item" style="text-align:center">
                        @csrf
                        @method('post')
                        <input hidden type="text" name="id" value="{{$foto->id}}">
                        <img width="40%" class="imagenPreview" idForPre="{{$foto->id}}" id="imagenPreview-{{$foto->id}}" src="{{asset($foto->url)}}" alt="">
                        <img class="imagenView" id="imagenView-{{$foto->id}}" src="{{asset($foto->url)}}" alt="">
                </li>
                @endforeach
            </ul>
        {{-- /Galeria --}}

        <div class="card-body">
            <a href="#" class="card-link"></a>
            <a href="{{route('product.list')}}" class="btn btn-secondary float-right">Volver</a>
        </div>
    </div>

</div>

@endsection
