@extends('layouts.main')
@section('title', 'Lista de productos')
@section('content')

<div style="margin:auto; padding: 0 5%">
    <a href="{{route('product.create')}}" style="margin-bottom: 0.5%" class="btn btn-success float-right">Nuevo</a>
    <table id="dataTables">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Foto</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Tarifa actual</th>
                <th scope="col">Categorias</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($products as $product)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td><img src="{{asset($product->foto)}}" alt="" width="50px"></td>
                <td>{{$product->nombre}}</td>
                <td>{{$product->descripcion}}</td>
                <td>
                  @php
                    $precioTotal = 0;
                  @endphp
                    @foreach ($product->tarifas as $tarifa)
                        @if($tarifa->fecha_inicial < now() && $tarifa->fecha_final > now())
                            @php
                              $precioTotal += $tarifa->precio;
                            @endphp
                        @endif
                    @endforeach
                    {{$precioTotal . '€'}}
                </td>

                <td>
                    @foreach ($product->categories as $cat)
                    {{$cat->nombre . ', '}}
                    @endforeach
                </td>
                <td>
                    <form class="" action="{{route('product.destroy')}}" method="POST">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{route('product.show', $product->id)}}" class="btn btn-secondary">Ver</a>
                            <a href="{{route('product.edit', $product->id)}}" class="btn btn-secondary">Editar</a>

                            @csrf
                            <input type="text" name="id" value="{{$product->id}}" hidden>
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                        </div>

                    </form>

                </td>

            </tr>
            @endforeach

        </tbody>
    </table>

</div>
@endsection
