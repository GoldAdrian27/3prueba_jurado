@extends('layouts.main')
@section('title', 'Crear nuevo producto')

@section('content')

<div style="margin:auto; width:40%">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form post_max_size="19967350" class="custom-forms" action="{{route('product.store')}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('POST')
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nombre</label>
            <div class="col-sm-10">
                <input type="text" name="nombre" value="{{old('nombre')}}" class="form-control" id="">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Descripción</label>
            <div class="col-sm-10">
                <input type="text" name="descripcion" value="{{old('descripcion')}}" class="form-control" id="">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Categorias</label>
            <div class="" style="margin: 30px 0">
                <input type="checkbox" class="form-check-input" id="seleccionarTodo" value="Seleccionar Todo">
                <label class="form-check-label" for="seleccionarTodo"> Seleccionar Todo</label>
            </div>
            <div class="form-check column-category">
                @foreach ($categories as $catGeneral)
                <div class="form-check form-check">
                    <input type="checkbox" name="categorias[]" class="form-check-input" id="inlineCheckbox{{$catGeneral->id}}" value="{{$catGeneral->id}}">
                    <label class="form-check-label" for="inlineCheckbox{{$catGeneral->id}}">{{$catGeneral->nombre}}</label>
                </div>
                @endforeach
            </div>
        </div>

        <span style="font-size:12px">Imagen de perfil</span>
        <div class="form-group">
            <input type="file" name="foto" class="form-control-file">
        </div>

        <span style="font-size:12px">Galeria de fotos</span>
        <div class="form-group">
            <input type="file" name="fotos[]" class="form-control-file" multiple>
        </div>


        <button type="submit" class="btn btn-success float-right">Crear</button>
        <a href="{{route('product.list')}}" class="btn btn-secondary float-left">Volver</a>

        {{-- <div class="form-group row">
    <div class="col-sm-10">
      <input type="date" class="form-control" id="inputDate3" >
    </div>
  </div> --}}
    </form>
</div>

@endsection
