//Comprobar en que pagina estoy
var page = $('.nav-treeview .nav-item .nav-link[href="' + window.location.href + '"]:first');
//Añadir la clase active para que resalte en la barra de navegacion
page.addClass("active")

//Comprueba que la fecha final es menor a la fecha inicial
$("#id_fecha_inicial").on("change", function() {
    var inicial = $(this).val();

    $("#id_fecha_final").attr('min', inicial);

    if (Date.parse(inicial) >= Date.parse($("#id_fecha_final").val())) {
        $("#id_fecha_final").val('');
    }
});



//Menu dropdown

$('#show-logged-options').on('click', function(){
  $('#logged-options').slideDown();



});

$(document).mouseup(function(e){
    var container = $("#logged-options");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        container.slideUp();
    }
});
